#!/bin/sh

TEMPLATE=$HOME/online-learning-code/src/lisa/learning.template.job
TMP_DIR=$HOME/tmp
OUT=irj_listwise_perfect
#OUT=irj_listwise_navigational
#OUT=irj_listwise_informational

NUM_RUNS=25
NUM_QUERIES=1000
USER_MODEL=environment.CascadeUserModel
SYSTEM=retrieval_system.ListwiseLearningSystem
COMPARISON="comparison.StochasticBalancedInterleave"
INIT_WEIGHTS=zero
DELTA=1
ALPHA=0.01
EXPERIMENTER=experiment.LearningExperiment
RANKER=ranker.DeterministicRankingFunction
RUNTIME=10
MINUTES=00
RUNDAYS=2



for DATA_SET in "MSLR" # "2003_hp_dataset" "2003_td_dataset" "2004_hp_dataset"
                       # "2004_np_dataset" "2004_td_dataset" "MQ2007" "MQ2008"
                       # "OHSUMED"
do
    FEATURE_COUNT=136 # MQ* - 46; MSLR - 136; Gov - 64 (letor 3);
    DATA_DIR="/home/khofmann/data/"
    # Y/MS-LR Navigational user_model_args 
    P_CLICK="\"0:0.05,1:0.3,2:0.5,3:0.7,4:0.95\""
    P_STOP="\"0:0.2,1:0.3,2:0.5,3:0.7,4:0.9\""
    FEATURE_COUNT=136 # MQ* - 46; MSLR - 136; Gov - 64 (letor 3);
    DATA_DIR="/home/khofmann/data/Gov/QueryLevelNorm"
    for COMPARISON_ARGS in "0.5" "0.4" "0.3" "0.2" "0.1"
    do
        JOB_FILE=$TMP_DIR/submit.$RANDOM.job
        cat $TEMPLATE | sed "s/##OUT##/$OUT-$COMPARISON/" | \
            sed "s!##DATA_DIR##!$DATA_DIR!" | \
            sed "s/##DATA_SET##/$DATA_SET/" | \
            sed "s/##FEATURE_COUNT##/$FEATURE_COUNT/" | \
            sed "s/##NUM_RUNS##/$NUM_RUNS/" | \
            sed "s/##NUM_QUERIES##/$NUM_QUERIES/" | \
            sed "s/##USER_MODEL##/$USER_MODEL/" | \
            sed "s/##P_CLICK##/$P_CLICK/" | \
            sed "s/##P_STOP##/$P_STOP/" | \
            sed "s/##SYSTEM##/$SYSTEM/" | \
            sed "s/##SYSTEM_ARGS##/$SYSTEM_ARGS/" | \
            sed "s/##RANKER##/$RANKER/" | \
            sed "s/##RANKER_ARGS##/$RANKER_ARGS/" | \
            sed "s/##INIT_WEIGHTS##/$INIT_WEIGHTS/" | \
            sed "s/##COMPARISON##/$COMPARISON/" | \
            sed "s/##COMPARISON_ARGS##/$COMPARISON_ARGS/" | \
            sed "s/##DELTA##/$DELTA/" | \
            sed "s/##ALPHA##/$ALPHA/" | \
            sed "s/##RUNDAYS##/$RUNDAYS/" | \
            sed "s/##RUNTIME##/$RUNTIME/" | \
            sed "s/##MINUTES##/$MINUTES/" | \
            sed "s/##EXPERIMENTER##/$EXPERIMENTER/" > $JOB_FILE
        echo "Submitting $JOB_FILE"
        qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
        sleep 2
    done
done



# Y/MS-LR Perfect user_model_args
# P_CLICK="\"0:0.0,1:0.2,2:0.4,3:0.8,4:1\""
# P_STOP="\"0:0.0,1:0.0,2:0.0,3:0.0,4:0.0\""
# Y/MS-LR Navigational user_model_args 
# P_CLICK="\"0:0.05,1:0.3,2:0.5,3:0.7,4:0.95\""
# P_STOP="\"0:0.2,1:0.3,2:0.5,3:0.7,4:0.9\""
# Y/MS-LR Informational user_model_args
# P_CLICK="\"0:0.4,1:0.6,2:0.7,3:0.8,4:0.9\""
# P_STOP="\"0:0.1,1:0.2,2:0.3,3:0.4,4:0.5\""
# perfect click model
# P_CLICK="\"0:0.0, 1:0.5, 2:1.0\""
# P_STOP="\"0:0.0, 1:0.0, 2:0.0\""
# navigational
# P_CLICK="\"0:0.05, 1:0.5, 2:0.95\""
# P_STOP="\"0:0.2, 1:0.55, 2:0.9\""
# informational
# P_CLICK="\"0:0.4, 1:0.65, 2:0.9\""
# P_STOP="\"0:0.1, 1:0.3, 2:0.5\""
# perfect click feedback
# P_CLICK="\"0:0.0, 1:1.0\""
# P_STOP="\"0:0.0, 1:0.0\""
# navigational
# P_CLICK="\"0:0.05, 1:0.95\""
# P_STOP="\"0:0.2, 1:0.9\""
# informational
# P_CLICK="\"0:0.4, 1:0.9\""
# P_STOP="\"0:0.1, 1:0.5\""

