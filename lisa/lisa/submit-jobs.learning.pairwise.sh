#!/bin/sh

TEMPLATE=$HOME/online-learning-code/src/lisa/learning.template.job
TMP_DIR=$HOME/tmp
OUT=irj_pairwise_test
#OUT=irj_pairwise_perfect
#OUT=irj_pairwise_navigational
#OUT=irj_pairwise_informational

NUM_RUNS=5
NUM_QUERIES=100
USER_MODEL=environment.CascadeUserModel
SYSTEM=retrieval_system.PairwiseLearningSystem
COMPARISON=""
INIT_WEIGHTS=zero
ALPHA=
DELTA=
ETA=0.001
LAMBDA=0.0

EXPERIMENTER=experiment.LearningExperiment
RANKER=ranker.DeterministicRankingFunction
RUNTIME=00
MINUTES=15

for DATA_SET in "2003_np_dataset"
#for DATA_SET in "2003_np_dataset" "2003_hp_dataset" "2003_td_dataset" "2004_hp_dataset" "2004_np_dataset" "2004_td_dataset" "MQ2007" "MQ2008" "OHSUMED"
do
    if [ "$DATA_SET" = "MQ2007" ] || [ "$DATA_SET" = "MQ2008" ] || [ "$DATA_SET" = "OHSUMED" ]
    then
        if [ "$DATA_SET" = "OHSUMED" ]
        then
            FEATURE_COUNT=45 # MQ* - 46; MSLR - 136; Gov - 64 (letor 3); OH - 45
        else
            FEATURE_COUNT=46 # MQ* - 46; MSLR - 136; Gov - 64 (letor 3);
        fi
        DATA_DIR="/home/khofmann/data/"
        # perfect click model
        P_CLICK="\"0:0.0, 1:0.5, 2:1.0\""
        P_STOP="\"0:0.0, 1:0.0, 2:0.0\""
        # navigational
        # P_CLICK="\"0:0.05, 1:0.5, 2:0.95\""
        # P_STOP="\"0:0.2, 1:0.55, 2:0.9\""
        # informational
        # P_CLICK="\"0:0.4, 1:0.65, 2:0.9\""
        # P_STOP="\"0:0.1, 1:0.3, 2:0.5\""
    else
        # LETOR 3.0 data sets (64 features, binary relevance)
        FEATURE_COUNT=64 # MQ* - 46; MSLR - 136; Gov - 64 (letor 3);
        DATA_DIR="/home/khofmann/data/Gov/QueryLevelNorm"
        # perfect click feedback
        P_CLICK="\"0:0.0, 1:1.0\""
        P_STOP="\"0:0.0, 1:0.0\""
        # navigational
        # P_CLICK="\"0:0.05, 1:0.95\""
        # P_STOP="\"0:0.2, 1:0.9\""
        # informational
        # P_CLICK="\"0:0.4, 1:0.9\""
        # P_STOP="\"0:0.1, 1:0.5\""
    fi
    for EPSILON in "0.0" "0.2" "0.4" "0.6" "0.8" "1.0"
    do
        SYSTEM_ARGS="\"--epsilon $EPSILON --eta $ETA --lamb $LAMBDA\""
        JOB_FILE=$TMP_DIR/submit.$RANDOM.job
        cat $TEMPLATE | sed "s/##OUT##/$OUT-$EPSILON/" | \
            sed "s!##DATA_DIR##!$DATA_DIR!" | \
            sed "s/##DATA_SET##/$DATA_SET/" | \
            sed "s/##FEATURE_COUNT##/$FEATURE_COUNT/" | \
            sed "s/##NUM_RUNS##/$NUM_RUNS/" | \
            sed "s/##NUM_QUERIES##/$NUM_QUERIES/" | \
            sed "s/##USER_MODEL##/$USER_MODEL/" | \
            sed "s/##P_CLICK##/$P_CLICK/" | \
            sed "s/##P_STOP##/$P_STOP/" | \
            sed "s/##SYSTEM##/$SYSTEM/" | \
            sed "s/##SYSTEM_ARGS##/$SYSTEM_ARGS/" | \
            sed "s/##RANKER##/$RANKER/" | \
            sed "s/##RANKER_ARGS##/$RANKER_ARGS/" | \
            sed "s/##INIT_WEIGHTS##/$INIT_WEIGHTS/" | \
            sed "s/##COMPARISON##/$COMPARISON/" | \
            sed "s/##COMPARISON_ARGS##/$COMPARISON_ARGS/" | \
            sed "s/##DELTA##/$DELTA/" | \
            sed "s/##ALPHA##/$ALPHA/" | \
            sed "s/##RUNTIME##/$RUNTIME/" | \
            sed "s/##MINUTES##/$MINUTES/" | \
            sed "s/##EXPERIMENTER##/$EXPERIMENTER/" > $JOB_FILE
        echo "Submitting $JOB_FILE"
        qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
        sleep 2
    done
done
