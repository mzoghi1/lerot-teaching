#!/bin/sh

TEMPLATE=$HOME/online-learning-code/src/lisa/sampling.template.job
TMP_DIR=$HOME/tmp
OUT=WSDM2015
#OUT=irj_listwise_navigational
#OUT=irj_listwise_informational

NUM_RUNS=32
NUM_QUERIES=1000000
USER_MODEL=environment.CascadeUserModel
SYSTEM=retrieval_system.SamplerSystem
COMPARISON=comparison.ProbabilisticInterleave
EXPERIMENTER=experiment.SamplingExperiment
RANKER=ranker.ProbabilisticRankingFunction
RANKER_ARGS=3
NR_RANKERS=136
RUNDAYS=5
RUNTIME=00
MINUTES=00
DATA_SET=MSLR
COMPARISON_ARGS=random
DATA_DIR=$HOME/online-learning-data/
UCBalpha=0.51

SAMPLER=BaselineSampler
FOLD=0
CLICK_MODEL=Perfect

if [ "$DATA_SET" = "MQ2007" ] || [ "$DATA_SET" = "MQ2008" ] || [ "$DATA_SET" = "OHSUMED" ]
then
    if [ "$DATA_SET" = "OHSUMED" ]
    then
        FEATURE_COUNT=45
        INIT_WEIGHTS=$HOME/online-learning-rankers/features45/
    else
        FEATURE_COUNT=46
        INIT_WEIGHTS=$HOME/online-learning-rankers/features46/
    fi
    if [ "$CLICK_MODEL" = "Perfect" ]
    then
        P_CLICK="\"0:0.0, 1:0.5, 2:1.0\""
        P_STOP="\"0:0.0, 1:0.0, 2:0.0\""
    elif [ "$CLICK_MODEL" = "Navigational" ]
    then
        P_CLICK="\"0:0.05, 1:0.5, 2:0.95\""
        P_STOP="\"0:0.2, 1:0.55, 2:0.9\""
    elif [ "$CLICK_MODEL" = "Informational" ]
    then
        P_CLICK="\"0:0.4, 1:0.65, 2:0.9\""
        P_STOP="\"0:0.1, 1:0.3, 2:0.5\""
    fi
elif [ "$DATA_SET" = "MSLR" ] || [ "$DATA_SET" = "Yandex" ] || [ "$DATA_SET" = "YLRSet1" ] || [ "$DATA_SET" = "YLRSet2" ]
then
    if [ "$DATA_SET" = "MSLR" ]
    then
        FEATURE_COUNT=136
    elif [ "$DATA_SET" = "Yandex" ]
    then
        FEATURE_COUNT=245
    elif [ "$DATA_SET" = "YLRSet1" ] || [ "$DATA_SET" = "YLRSet2" ]
    then
        FEATURE_COUNT=700
    fi
    
    INIT_WEIGHTS=$HOME/online-learning-rankers/features$FEATURE_COUNT/

    if [ "$CLICK_MODEL" = "Perfect" ]
    then
        P_CLICK="\"0:0.0,1:0.25,2:0.5,3:0.75,4:1.0\""
        P_STOP="\"0:0.0,1:0.0,2:0.0,3:0.0,4:0.0\""
    elif [ "$CLICK_MODEL" = "Navigational" ]
    then
        P_CLICK="\"0:0.05,1:0.3,2:0.5,3:0.7,4:0.95\""
        P_STOP="\"0:0.2,1:0.3,2:0.5,3:0.7,4:0.9\""
    elif [ "$CLICK_MODEL" = "Informational" ]
    then
        P_CLICK="\"0:0.4,1:0.6,2:0.7,3:0.8,4:0.9\""
        P_STOP="\"0:0.1,1:0.2,2:0.3,3:0.4,4:0.5\""
    fi
else
    # LETOR 3.0 data sets (64 features, binary relevance)
    FEATURE_COUNT=64 # MQ* - 46; MSLR - 136; Gov - 64 (letor 3);
    INIT_WEIGHTS=$HOME/online-learning-rankers/features64/
    if [ "$CLICK_MODEL" = "Perfect" ]
    then
        P_CLICK="\"0:0.0, 1:1.0\""
        P_STOP="\"0:0.0, 1:0.0\""
    elif [ "$CLICK_MODEL" = "Navigational" ]
    then
        P_CLICK="\"0:0.05, 1:0.95\""
        P_STOP="\"0:0.2, 1:0.9\""
    elif [ "$CLICK_MODEL" = "Informational" ]
    then
        P_CLICK="\"0:0.4, 1:0.9\""
        P_STOP="\"0:0.1, 1:0.5\""
    fi
fi

for ind in {1..100} # {1..100}
do
    STAMP=`echo $(date +"%D %T") | sed "s/[^0-9]/_/g"`
    JOB_FILE=$TMP_DIR/submit.$STAMP.job
    cat $TEMPLATE | sed "s|##OUT##|$OUT/$DATA_SET-$NR_RANKERS-$CLICK_MODEL/$STAMP|" | \
        sed "s|##DATA_DIR##|$DATA_DIR|" | \
        sed "s|##DATA_SET##|$DATA_SET|" | \
        sed "s|##FOLD##|$FOLD|" | \
        sed "s|##FEATURE_COUNT##|$FEATURE_COUNT|" | \
        sed "s|##NUM_RUNS##|$NUM_RUNS|" | \
        sed "s|##NUM_QUERIES##|$NUM_QUERIES|" | \
        sed "s|##USER_MODEL##|$USER_MODEL|" | \
        sed "s|##P_CLICK##|$P_CLICK|" | \
        sed "s|##P_STOP##|$P_STOP|" | \
        sed "s|##SYSTEM##|$SYSTEM|" | \
        sed "s|##SYSTEM_ARGS##|$SYSTEM_ARGS|" | \
        sed "s|##NR_RANKERS##|$NR_RANKERS|" | \
        sed "s|##RANKER##|$RANKER|" | \
        sed "s|##RANKER_ARGS##|$RANKER_ARGS|" | \
        sed "s|##INIT_WEIGHTS##|$INIT_WEIGHTS|" | \
        sed "s|##COMPARISON##|$COMPARISON|" | \
        sed "s|##COMPARISON_ARGS##|$COMPARISON_ARGS|" | \
        sed "s|##SAMPLER##|$SAMPLER|" | \
        sed "s|##UCB_Alpha##|$UCBalpha|" | \
        sed "s|##RUNDAYS##|$RUNDAYS|" | \
        sed "s|##RUNTIME##|$RUNTIME|" | \
        sed "s|##MINUTES##|$MINUTES|" | \
        sed "s|##EXPERIMENTER##|$EXPERIMENTER|" > $JOB_FILE
    echo "Submitting $JOB_FILE"
    cat $JOB_FILE
    qsub -e$JOB_FILE.e -o$JOB_FILE.o $JOB_FILE
    sleep 2
done

# Y/MS-LR Perfect user_model_args
# P_CLICK="\"0:0.0,1:0.2,2:0.4,3:0.8,4:1\""
# P_STOP="\"0:0.0,1:0.0,2:0.0,3:0.0,4:0.0\""
# Y/MS-LR Navigational user_model_args 
# P_CLICK="\"0:0.05,1:0.3,2:0.5,3:0.7,4:0.95\""
# P_STOP="\"0:0.2,1:0.3,2:0.5,3:0.7,4:0.9\""
# Y/MS-LR Informational user_model_args
# P_CLICK="\"0:0.4,1:0.6,2:0.7,3:0.8,4:0.9\""
# P_STOP="\"0:0.1,1:0.2,2:0.3,3:0.4,4:0.5\""
# perfect click model
# P_CLICK="\"0:0.0, 1:0.5, 2:1.0\""
# P_STOP="\"0:0.0, 1:0.0, 2:0.0\""
# navigational
# P_CLICK="\"0:0.05, 1:0.5, 2:0.95\""
# P_STOP="\"0:0.2, 1:0.55, 2:0.9\""
# informational
# P_CLICK="\"0:0.4, 1:0.65, 2:0.9\""
# P_STOP="\"0:0.1, 1:0.3, 2:0.5\""
# perfect click feedback
# P_CLICK="\"0:0.0, 1:1.0\""
# P_STOP="\"0:0.0, 1:0.0\""
# navigational
# P_CLICK="\"0:0.05, 1:0.95\""
# P_STOP="\"0:0.2, 1:0.9\""
# informational
# P_CLICK="\"0:0.4, 1:0.9\""
# P_STOP="\"0:0.1, 1:0.5\""